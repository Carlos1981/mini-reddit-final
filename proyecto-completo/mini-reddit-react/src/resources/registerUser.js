import { host } from '../config';
import { throwError } from '../components/Utilities';

const registerUser = async (name, password, email) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password, name }),
  };
  const res = await fetch(`${host}/user`, requestOptions);
  const bodyMessages = await res.json();

  if (res.ok) {
    return bodyMessages;
  } else {
    throwError(res.status, bodyMessages.message);
  }
};

export { registerUser };

import { host } from '../config';
import { throwError } from '../components/Utilities';

const getUserProfile = async (token, id, setProfileData) => {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json', Authorization: token },
  };
  const res = await fetch(`${host}/user/${id}`, requestOptions);
  const bodyMessages = await res.json();

  if (res.ok) {
    setProfileData(bodyMessages.data.user);
    return res;
  } else {
    throwError(res.status, bodyMessages.message);
  }
};

export { getUserProfile };

import { host } from '../config';

const vote = async (postId, value, token) => {
  try {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ value }),
    };
    const resVote = await fetch(`${host}/post/${postId}/vote`, requestOptions);
    const bodyRes = await resVote.json();
    if (resVote.ok) {
      return bodyRes.data.value;
    } else {
      return bodyRes;
    }
  } catch (err) {
    console.error(err);
  }
};

export { vote };

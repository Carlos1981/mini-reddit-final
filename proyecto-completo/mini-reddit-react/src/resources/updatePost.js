import { host } from '../config';
import { throwError } from '../components/Utilities';

const updatePost = async (token, title, content, id) => {
  let finalBody;

  finalBody = JSON.stringify({ title, content });

  try {
    const requestOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: finalBody,
    };
    const resPost = await fetch(`${host}/post/${id}`, requestOptions);
    const bodyRes = await resPost.json();
    if (resPost.ok) {
      return bodyRes;
    } else {
      throwError(resPost.status, bodyRes.message);
    }
  } catch (err) {
    console.error(err);
  }
};

export { updatePost };
